<?php

use App\partidas;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Str;
use app\PartidasController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/create-game', 'PartidasController@newGame');
Route::get('/unirse-game', 'PartidasController@joinGame');

Route::get('/leer', function() 
{
    $data = partidas::all('uuid');
    foreach($data as $e)
    {
        echo "<p> uuid: ".$e->uuid." | nombre de jugador: ". $e->name."</p>";
    }
});

Route::get('/filtro_uuid', function() 
{
    $data = partidas::where('name','eeerff')->get();
    if(!empty($data[0]))
    {
        return 
        [
            $data[0]->uuid, $data[0]->id, $data[0]->updated_at
        ];
    }
    return view('layouts.heders');
    // echo "<p> name: ".$data->name" | updated_at: ". $data->updated_at."</p>";
});

Route::get('/insert_uuid', function()
{
    return "";
});