<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <!-- <link rel="stylesheet" href="../../../public/css/main.css"> -->
  <title>newGame</title>
  <style>
    html, body 
    {
      box-sizing: border-box;
      background-color: #fff;
      color: #636b6f;
      font-family: 'Nunito', sans-serif;
      font-weight: 200;
      height: 100vh;
      margin: 0;
      min-width: 285px;
    }

    .full-height 
    {
      height: 90vh;
    }

    .position-ref 
    {
      position: relative;
    }

    .flex-center 
    {
      align-items: center;
      display: flex;
      justify-content: center;
    }

    .title 
    {
      font-size: 30px;
    }

    .tic
    {
      border: 2px solid rgba(0,195,253,1);
      width: 3rem;
      height: 3rem;
      display: flex;
      justify-content: center;
      align-items: center;
    }

    .grid
    {
      margin: auto;
      width: 10rem;
      display: grid;
      grid-template-columns: 1fr 1fr 1fr;
      grid-template-rows: 1fr 1fr 1fr;
    }
    
    .jugador, .turno, .uuid
    {
      text-align: center;
    }

    .btn_tic__tac
    {
      width: 100%;
      height: 100%;
    }
  </style>
</head>
<body>
  <div class="flex-center position-ref">
    @yield('titleheader')
  </div>
  
  <div class="start_game grid" id="tic-tac">
      @yield('game')
      <?php
        use Illuminate\Support\Str;
        $box = [1,2,3,4,5,6,7,8,9];
        $turno = 0;
        $tiempo = now();
      ?>
      @foreach($box as $e)
          <div class="tic" id="{{$e}}">
            <button class="btn_tic__tac" id="{{$e}}"></button>
          </div>
      @endforeach
  </div>

  <div class="data">
    @yield('data')
    <h3 class="jugador">juagador 1</h3>
    <p class="turno">turnos: {{$turno}}</p>
    <p class="uuid">id: {{$uuid}}</p>
    
  </div>

  <div class="footer">
    @yield('footer')
  </div>

  <!-- <script src="../../../public/js/validarPartida.js">

  </script> -->
</body>
</html>