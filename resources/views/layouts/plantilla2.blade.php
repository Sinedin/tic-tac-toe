<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <!-- <link rel="stylesheet" href="../../../public/css/main.css"> -->
  <title>newGame</title>
  <style>
    html, body 
    {
      box-sizing: border-box;
      background-color: #fff;
      color: #636b6f;
      font-family: 'Nunito', sans-serif;
      font-weight: 200;
      height: 100vh;
      margin: 0;
    }

    .btn_send
    {
      border: 2px solid rgba(0,195,253,1);
      border-radius: .5rem;
      display: block;
      font-size: 12px;
      margin: 1rem auto;
      width: 18%;
      padding: .5rem;
      box-shadow: 5px 5px 10px rgba(80,81,85,.9),-5px 0 15px rgba(80,81,85,.9);
      text-decoration: none;
      transition: all 5s;
    }

    .btn_send:hover
    {
      text-decoration:underline;
    }

    .content 
    {
      text-align: center;
      min-width: 235px;
    }

    .content .title 
    {
      font-size: 25px;
    }
    .content .form_two .inp_user_two
    {
      border: 1px solid rgba(0,195,253,1);
      border-radius: 3px;
      display: inline-block;
      font-size: 12px;
      padding: 5px;
    }
  </style>
</head>
<body>
  <div class="content">
    @yield('unirse_partida')
      <h1 class="title ">Ingrese su ID</h1>
      <form action="POST" class="form_two">
          <input class="inp_user_two" type="text" id="user_two" placeholder="Usuario 2">
          
          <input class="inp_user_two" type="text" name="" id="UUID" placeholder="ingrese su ID">
      </form>
      <a class="btn_send" href="#">
        ENVIAR
      </a>
  </div>
</body>
</html>